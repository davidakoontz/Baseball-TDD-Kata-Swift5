#  Baseball-TDD-Kata-Swift5 Release v1.0 Starter Project

## This release has some very very basic Baseball Object Model Classes:

- Play.swift - a message and data that gets passed around to determine the outcome
- Situation.swift - game situation # of Outs;  Balls & Strikes; etc.
- FieldSimulation.swift - definition of the field we are playing upon e.g. fence depth
- PlayGenerator.swift - to create randomize play
- Scorecard.swift - I'd like to see a classic score keepers card output for each at-Bat.

Along with some very basic TDD derived XCTest Cases - all written in XCode with Swift 5.

- Baseball-TDDTests folder
  - Baseball_TDDTests.swift
  
User Interface Test using SwiftUI 5.3 & XCode 12 
- Baseball-TDDUITests folder
 - Baseball_TDDUITests.swift

### Start here and start writing test cases - implementation code - some UI - in SwiftUI and you can have a great learning experience.

### Copyright (c) 2020 TheSwiftDojo.com  & David A. Koontz

