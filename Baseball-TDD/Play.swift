//
//  Play.swift
//  Baseball-TDD
//
//  Created by David on 10/4/20.
//

import Foundation

public class Play {
    public let number: Int
    public let description: String
    
    init() {
        self.number = 1
        self.description = "a very beginging play in baseball"
    }
}
