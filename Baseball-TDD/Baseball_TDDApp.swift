//
//  Baseball_TDDApp.swift
//  Baseball-TDD
//
//  Created by David on 10/4/20.
//

import SwiftUI

@main
struct Baseball_TDDApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
